using System;
using ChatServer.Network.Manager;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace ChatServer.Tests
{
    public class NetworkManagerFactoryTest
    {
        private readonly IServiceProvider _services = new ServiceProviderBuilder().Build();

        [Fact]
        public void Build () 
        {
            //Arrange
            NetworkManagerFactory factory = _services.GetRequiredService<NetworkManagerFactory>();

            //Act
            INetworkManager manager = factory.Build("Tcp");

            //Assert
            Assert.IsType<TcpNetworkManager>(manager);
        }
    }
}