using System;
using ChatServer.ChatRoom;
using ChatServer.Commands;
using ChatServer.Extensions.Command;
using ChatServer.Extensions.Network;
using ChatServer.Network;
using ChatServer.Network.Manager;
using ChatServer.Server;
using ChatServer.ServerClient;
using Microsoft.Extensions.DependencyInjection;

namespace ChatServer.Tests
{
    public class ServiceProviderBuilder
    {
        private IServiceCollection Collection { get; }
        
        public ServiceProviderBuilder ()
        {
            Collection = new ServiceCollection()
                .AddLogging()
                .AddCommands()
                .AddNetworkManagers()
                .AddSingleton<ClientHandler>()
                .AddSingleton<RoomHandler>()
                .AddScoped<CommandFactory>()
                .AddScoped<NetworkManagerFactory>()
                .AddScoped<PendingNetworkClientsQueue>()
                .AddScoped<MessageSender>();
        }

        public IServiceProvider Build ()
        {
            return Collection.BuildServiceProvider();
        }
    }
}