using System.Net.Sockets;
using ChatServer.Network;
using System;
using ChatServer.Network.NetworkClient;
using Xunit;

namespace ChatServer.Tests
{
    public class PendingSocketsQueueTest
    {
        //Flag related to OnEnqueueEvent_Is_Invoked test
        //This is used to indicate whether or not the Enqueue event was invoked
        //when a socket was Qneueued.
        private bool eventWasInvoked = false;

        //Method reated to OnEnqueueEvent_Is_Invoked test
        private void OnEnqueueEvent (object sender, EventArgs e) 
        { 
            eventWasInvoked = true;
        }

        [Fact]
        public void Dequeuing_Empty_Queue_Returns_Null ()
        {
            //Arrange
            PendingNetworkClientsQueue queue = new PendingNetworkClientsQueue();

            //Act
            INetworkClient sock = queue.Dequeue();

            //Assert
            Assert.Null(sock);
        }

        [Fact]
        public void Dequeueing_Queue_Returns_Socket () 
        {
            //Arrange
            PendingNetworkClientsQueue queue = new PendingNetworkClientsQueue();

            INetworkClient socketToEnqueue = new TcpNetworkClient(new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp));
            queue.Enqueue(socketToEnqueue);

            //Act
            INetworkClient dequeuedSocket = queue.Dequeue();

            //Assert
            Assert.Equal(socketToEnqueue, dequeuedSocket);
        }

        [Fact]
        public void OnEnqueueEvent_Is_Invoked () 
        {
            //Arrange
            PendingNetworkClientsQueue queue = new PendingNetworkClientsQueue();
            queue.EnqueueEvent += OnEnqueueEvent;

            //Act
            INetworkClient socketToEnqueue = new TcpNetworkClient(new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp));
            queue.Enqueue(socketToEnqueue);

            //Assert
            Assert.True(eventWasInvoked);
        }
    }
}
