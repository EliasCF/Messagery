using Microsoft.Extensions.DependencyInjection;
using ChatServer.Server;
using System;
using ChatServer.Commands;
using ChatServer.Commands.Implementation.Client;
using Xunit;

namespace ChatServer.Tests
{
    public class CommandFactoryTest
    {
        private readonly IServiceProvider _services = new ServiceProviderBuilder().Build();

        [Fact]
        public void Build () 
        {
            //Arrange
            CommandFactory factory = _services.GetRequiredService<CommandFactory>();

            //Act
            ICommand nameCommand = factory.Build("/Name John Doe");

            //Assert
            Assert.IsType<NameCommand>(nameCommand);
        }
    }
}