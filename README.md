# Messagery Chatserver
##### This is Messagery. An asynchronous TCP chat server.



## Server settigs
The server has serveral different configurable settings. These settings can be found in the appsettings.json file.
#### Strict
ServerConfigurations.Strict define if client created resources like chat rooms should be removed of as soon as they are unused.
#### Port
In ServerConfigurations.Port you can configure port settings.
The default port is 7777.
#### NetworkOption
ServerConfiguration.NetworkOption tells the server what protocol to use for communication with its clients.
The currently supported protocols are:   
* ```"NetworkOption": "Tcp"```   
* ```"NetworkOption": "Ssl"```
#### Security
The security settings are used to configure security related settigs.
These settings do not need to be configured if the server doesn't use the SSL protocol for communication.
##### CertificatePath
When NetworkOption has been set to Ssl you must have a certfiicate in order to encrypt the communication with your clients.
ServerConfiguration.Security.CertificatePath is the setting in which you tell the server where it can find the certficate.
##### CertificatePassword 
ServerConfiguration.Security.CertificatePath is the setting for telling the server what the certificate password is.


## How to connect with client
Messagery doesn't have any dedicated client application yet, but any TCP client should do just.
The server supports both TCP and SSL communication.
##### Connecting with TCP
The TCP example will use the netcat networking utility. 
```nc <ip> <port>```
##### Connecting with SSL
The Ssl exmaple will use the openssl CLI.
```openssl s_client -connect <ip>:<port>```


## Running server in Docker
##### This section will instruct you in how to get this project up and running in a Docker container

Make app ready for deployment  
```dotnet publish -c Release```

Build Docker image  
```docker build -t chatserver -f Dockerfile .```

Create docker container from Docker image  
```docker create --name chat chatserver```

Make sure container has been created  
```docker ps -a```

Run container and attach STDOUT  
```docker start --attach name```



## List of commands

##### These are the definitions of the commands, but the server ignores case when validating commands, meaning that the first letter isn't required to be uppercase.
#### General commands
##### Set name
Sets clients name. This is the first command a client will need to send to the server after connecting.   
```/Name <name>```
##### Disconnect
Disconnect from server   
```/Disc```

#### Chat room commands
##### Create room
Create a room with a given name and join the room.   
```/RoomCreate <Room name>```
##### Join Room
Join room with a given name.   
```/RoomJoin <Room name>```
##### Leave room
Leave the room you are curently in.   
```/RoomDisc```
##### Room overview
Get list of all existing rooms   
```/RoomOverview```

#### Information commands
##### Client count
Get the number of clients currently connected to the server   
```/ClientCount```

## Generaton of certificate:

```
openssl req -x509 -out localhost.crt -keyout localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```

```openssl pkcs12 -export -inkey localhost.key -in localhost.crt -out cert.p12```