FROM mcr.microsoft.com/dotnet/core/runtime:3.0

COPY ./bin/Release/netcoreapp3.0/publish/ .

EXPOSE 7777

ENTRYPOINT ["dotnet", "./ChatServer.dll"]