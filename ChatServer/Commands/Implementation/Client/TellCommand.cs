using System.Linq;
using ChatServer.Server;
using ChatServer.ServerClient;

namespace ChatServer.Commands.Implementation.Client
{
    [OutputIgnorable]
    [CompatibleClientStates(ClientState.Passive)]
    public class TellCommand : ICommand
    {
        public string Command => "/Tell";

        private ClientHandler Clients { get; }

        private MessageSender Sender { get; }

        public TellCommand (
            ClientHandler clients,
            MessageSender sender)
        {
            Clients = clients;
            Sender = sender;
        }

        public void Handle(ServerClient.Client client, string parameter = null)
        {
            if (parameter == null) return;

            if (parameter == string.Empty) 
            {
                Sender.Send(client, "You must provide parameters\n\r");
                return;
            }

            string[] parameters = parameter.Split(" ", 2);

            ServerClient.Client receiver = Clients.GetByName(parameters.ElementAt(0));

            if (receiver == null)
            {
                Sender.Send(client, $"Client: {parameters.ElementAt(0)}, not found\n\r");
                return;
            }

            if (string.IsNullOrWhiteSpace(parameters.ElementAt(1))) 
            {
                Sender.Send(client, $"You must specificy a message when telling {receiver.Name} something");
                return;
            }

            Sender.Send(receiver, $"{client.Name} tells you: {parameters.ElementAt(1)}\n\r");
        }
    }
}