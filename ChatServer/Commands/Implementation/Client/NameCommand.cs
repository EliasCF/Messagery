using System.Linq;
using ChatServer.Server;
using ChatServer.ServerClient;

namespace ChatServer.Commands.Implementation.Client
{
    [OutputIgnorable]
    [CompatibleClientStates(ClientState.NeedName)]
    public class NameCommand : ICommand
    {
        public string Command { get; } = "/Name";

        private ClientHandler Clients { get; }

        private MessageSender Sender { get; }

        public NameCommand () { }

        public NameCommand (
            ClientHandler clients,
            MessageSender sender)
        {
            Clients = clients;
            Sender = sender;
        }

        public void Handle(ServerClient.Client client, string parameter = null) 
        {
            if (parameter == null) return;

            if (parameter == string.Empty) 
            {
                Sender.Send(client, "Your name cannot be empty\n\r");
                return;
            }

            if (parameter.Any(x => char.IsWhiteSpace(x)))
            {
                Sender.Send(client, "Names cannot contain spaces\n\r");
                return;
            }

            string clientName = parameter;

            //Set name of client if the name isn't taken
            if (Clients.Exists(clientName))
            {
                string message = $"The name {clientName} is taken, pick a different name.\n\r";
                Sender.Send(client, message);
            }
            else
            {
                Clients.SetName(client.Id, clientName);
                Clients.SetState(client.Id, ClientState.Passive);
                
                Sender.Send(client, $"Your name has been set to '{clientName}'\n\r");
                Sender.SendToAll(null, $"{clientName} has joined the chat", true);
            }
        }
    }
}