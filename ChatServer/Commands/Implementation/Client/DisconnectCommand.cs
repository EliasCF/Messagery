using ChatServer.Extensions.Command;
using ChatServer.Network;
using ChatServer.Server;
using ChatServer.ServerClient;

namespace ChatServer.Commands.Implementation.Client
{
    [OutputIgnorable]
    [CompatibleClientStates(ClientState.Passive, ClientState.NeedName)]
    public class DisconnectCommand : ICommand
    {
        public string Command { get; } = "/Disc";

        private ClientHandler Clients { get; }

        private MessageSender Sender { get; }

        public DisconnectCommand (
            ClientHandler clients,
            MessageSender sender
        ) 
        { 
            Clients = clients;
            Sender = sender;
        }

        public void Handle (ServerClient.Client client, string parameter = null) 
        {
            if (parameter.ContainsParameters(Sender, client, Command)) return;

            string message =  $"{client.Name} left the chat\n\r";

            Clients.Close(client.Id);
            Sender.SendToAll(null, message, true);
        }
    }
}