using ChatServer.ServerClient;

namespace ChatServer.Commands.Implementation
{
    /// <summary>
    /// A command that does nothing
    /// </summary>
    [CompatibleClientStates]
    public class NoCommand : ICommand
    {
        public string Command { get; } = "";

        public NoCommand () { }

        public void Handle(ServerClient.Client client, string parameter = null) { }
    }
}