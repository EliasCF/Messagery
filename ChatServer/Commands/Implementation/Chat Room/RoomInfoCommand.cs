using ChatServer.Extensions.Command;
using System.Collections.Generic;
using ChatServer.ServerClient;
using ChatServer.ChatRoom;
using ChatServer.Server;
using System;

namespace ChatServer.Commands.Implementation.Chat_Room
{
    [CompatibleClientStates(ClientState.Passive)]
    public class RoomInfoCommand : ICommand
    {
        public string Command => "/RoomInfo";

        private RoomHandler ChatRooms { get; }

        private MessageSender Sender { get; }

        private ClientHandler Clients { get; }

        public RoomInfoCommand (
            RoomHandler chatRooms,
            MessageSender sender,
            ClientHandler clients)
        {
            ChatRooms = chatRooms;
            Sender = sender;
            Clients = clients;
        }

        public void Handle(ServerClient.Client client, string parameter = null)
        {
            if (parameter.ContainsParameters(Sender, client, Command)) return;
            
            if (client.RoomId == Guid.Empty) 
            {
                Sender.Send(client, "You are not in a room");
                return;
            }

            Room room = ChatRooms.FindRoomById(client.RoomId);
            string roomCreator = Clients.GetId(room.CreatedBy).Name;

            List<string> infoList = new List<string> 
            {
                $"Room name: {room.Name}",
                $"Room creator: {roomCreator}",
                $"Time created: {room.CreationTime}",
                $"Users in room: {room.ClientCount}"
            };

            string roomInfo = $"=====\nRooms info\n=====\n{string.Join("\n", infoList)}\n=====\n\r";
            Sender.Send(client, roomInfo);
        }
    }
}