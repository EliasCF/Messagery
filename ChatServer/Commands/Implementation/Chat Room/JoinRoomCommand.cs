using System;
using ChatServer.ChatRoom;
using ChatServer.Network;
using ChatServer.Server;
using ChatServer.ServerClient;

namespace ChatServer.Commands.Implementation.Chat_Room
{
    [CompatibleClientStates(ClientState.Passive)]
    public class JoinRoomCommand : ICommand
    {
        public string Command { get; } = "/RoomJoin";

        private ClientHandler Clients { get; }
        
        private RoomHandler ChatRooms { get; }

        private MessageSender Sender { get; }

        public JoinRoomCommand () { }

        public JoinRoomCommand (
            ClientHandler clients,
            RoomHandler chatRooms,
            MessageSender sender
        )
        {
            Clients = clients;
            ChatRooms = chatRooms;
            Sender = sender;
        }

        public void Handle(ServerClient.Client client, string parameter = null)
        {
            if (parameter == null) return;

            if (!Clients.Exists(client.Id)) return;
            
            string roomName = parameter;

            Guid roomId = ChatRooms.FindByName(roomName);

            if (roomId == Guid.Empty) 
            {
                Sender.Send(client, $"Room '{roomName}', not found\n\r");
                return;
            }
            
            Clients.SetRoom(client.Id, roomId);
            ChatRooms.IncrementClientCount(roomId);

            string message = $"{client.Name} has joined room: {roomName}\n\r";
            Sender.SendToAll(null, message, true);
        }
    }
}
