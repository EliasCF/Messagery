using System.Collections.Generic;
using System.Linq;
using ChatServer.ChatRoom;
using ChatServer.Extensions.Command;
using ChatServer.Network;
using ChatServer.Server;
using ChatServer.ServerClient;

namespace ChatServer.Commands.Implementation.Chat_Room
{
    [CompatibleClientStates(ClientState.Passive)]
    public class RoomOverviewCommand : ICommand
    {
        public string Command { get; } = "/RoomOverview";

        private RoomHandler Rooms { get; }

        private MessageSender Sender { get; }

        public RoomOverviewCommand () { }

        public RoomOverviewCommand (
            RoomHandler rooms,
            MessageSender sender)
        {
            Rooms = rooms;
            Sender = sender;
        }

        public void Handle (ServerClient.Client client, string parameter = null) 
        {
            if (parameter.ContainsParameters(Sender, client, Command)) return;

            IEnumerable<string> _roomsList = Rooms.GetAll().Select(room => room.Name);
            string roomNames = $"=====\n_Rooms overview:\n{string.Join("\n", _roomsList)}\n=====\n\r";

            Sender.Send(client, roomNames);
        }
    }
}