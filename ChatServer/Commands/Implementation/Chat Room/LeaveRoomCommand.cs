using System;
using ChatServer.ChatRoom;
using ChatServer.Configuration;
using ChatServer.Extensions.Command;
using ChatServer.Network;
using ChatServer.Server;
using ChatServer.ServerClient;
using Microsoft.Extensions.Options;

namespace ChatServer.Commands.Implementation.Chat_Room
{
    [CompatibleClientStates(ClientState.Passive)]
    public class LeaveRoomCommand : ICommand
    {
        public string Command { get; } = "/RoomDisc";

        private ClientHandler Clients  { get; }

        private MessageSender Sender { get; }
        
        private RoomHandler ChatRooms { get; }
        
        private bool Strict { get; }

        public LeaveRoomCommand () { }

        public LeaveRoomCommand (
            ClientHandler clients,
            MessageSender sender,
            RoomHandler chatRooms,
            IOptions<ServerConfigurations> configs) 
        {
            Clients = clients;
            Sender = sender;
            ChatRooms = chatRooms;
            Strict = configs.Value.Strict;
        }

        public void Handle (ServerClient.Client client, string parameter = null) 
        {
            if (parameter.ContainsParameters(Sender, client, Command)) return;

            string message = $"{client.Name} left the room\n\r";

            Sender.SendToAll(null, message, true);

            Guid oldRoomId = client.RoomId;
            ChatRooms.DecrementClientCount(oldRoomId);
            
            Clients.SetRoom(client.Id, Guid.Empty);

            if (Strict)
            {
                if (ChatRooms.GetClientCount(oldRoomId) > 0) return;
                
                ChatRooms.Remove(oldRoomId);
            }
        }
    }
}