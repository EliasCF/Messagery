using System;
using System.Linq;
using ChatServer.ChatRoom;
using ChatServer.Configuration;
using ChatServer.Network;
using ChatServer.Server;
using ChatServer.ServerClient;
using Microsoft.Extensions.Options;

namespace ChatServer.Commands.Implementation.Chat_Room
{
    [CompatibleClientStates(ClientState.Passive)]
    public class CreateRoomCommand : ICommand
    {
        public string Command { get; } = "/RoomCreate";

        private ClientHandler Clients { get; }

        private RoomHandler ChatRooms { get; }

        private MessageSender Sender { get; }
        
        private bool Strict { get; }

        public CreateRoomCommand () { }

        public CreateRoomCommand (
            ClientHandler clients,
            RoomHandler chatRooms,
            MessageSender sender,
            IOptions<ServerConfigurations> configs)
        {
            Clients = clients;
            ChatRooms = chatRooms;
            Sender = sender;
            Strict = configs.Value.Strict;
        }

        public void Handle(ServerClient.Client client, string parameter = null) 
        {
            if (parameter == null) return;
            
            if (parameter == string.Empty)
            {
                Sender.Send(client, "Room name cannot be empty\n\r");
                return;
            }

            if (parameter.Any(x => char.IsWhiteSpace(x)))
            {
                Sender.Send(client, "Room names cannot contain spaces\n\r");
                return;
            }

            string roomName = parameter;

            Guid oldRoomId = client.RoomId;

            Sender.Send(client, $"You created a new room: '{roomName}'\n\r");

            Guid newRoom = ChatRooms.Add(roomName, client.Id);
            ChatRooms.DecrementClientCount(oldRoomId);
            
            Clients.SetRoom(client.Id, ChatRooms.FindByName(roomName));
            ChatRooms.IncrementClientCount(newRoom);
            
            if (Strict)
            {
                if (ChatRooms.GetClientCount(oldRoomId) > 0) return;
                ChatRooms.Remove(oldRoomId);
            }
        }
    }
}