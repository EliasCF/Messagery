using ChatServer.Extensions.Command;
using ChatServer.Server;
using ChatServer.ServerClient;

namespace ChatServer.Commands.Implementation.Info
{
    [CompatibleClientStates(ClientState.Passive)]
    public class ClientCountCommand : ICommand
    {
        public string Command { get; } = "/ClientCount";

        private MessageSender Sender  { get; }

        private ClientHandler Clients { get; }

        public ClientCountCommand () { }

        public ClientCountCommand (
            MessageSender sender,
            ClientHandler clients) 
        {
            Sender = sender;
            Clients = clients;
        }

        public void Handle (ServerClient.Client client, string parameter = null) 
        {
            if (parameter.ContainsParameters(Sender, client, Command)) return;

            string message =  $"Current amount of server clients: {Clients.Count}\n\r";

            Sender.SendToAll(null, message, true);
        }
    }
}