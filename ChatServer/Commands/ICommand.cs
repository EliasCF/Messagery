using ChatServer.ServerClient;

namespace ChatServer.Commands
{
    public interface ICommand
    {
        /// <summary>
        /// Contains the functionality of the command
        /// </summary>
        /// <param name="client">The client that send the command</param>
        /// <param name="parameter">Command parameters</param>
        void Handle(Client client, string parameter = null);
        
        /// <summary>
        /// Contain the command as text.
        /// It will be used to validate against any potential command a user might send
        /// </summary>
        string Command { get; }
    }
}