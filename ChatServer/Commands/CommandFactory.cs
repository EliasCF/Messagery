using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ChatServer.Commands.Implementation;

namespace ChatServer.Commands
{
    public class CommandFactory
    {
        private IEnumerable<ICommand> Commands { get; }

        public CommandFactory (IEnumerable<ICommand> commands)
        {
            Commands = commands;
        }

        public ICommand Build (string userInput) 
        {
            string commandName = userInput.Split(' ')[0];

            ICommand command = Commands.SingleOrDefault(c =>
            {
                PropertyInfo commandProperty = c.GetType().GetProperty("Command");

                if (commandProperty == null) return false;
                
                return commandProperty
                    .GetValue(c, null)
                    .ToString()
                    .Equals(commandName, StringComparison.OrdinalIgnoreCase);
            });

            return command ?? Commands.OfType<NoCommand>().First();
        }
    }
}