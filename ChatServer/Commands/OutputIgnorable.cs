using System;

namespace ChatServer.Commands
{   
    /// <summary>
    /// Will be placed on ICommand implementations to indicate that the MessageSender shouldn't broadcast the command message itself
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class OutputIgnorable : Attribute
    {
        
    }
}