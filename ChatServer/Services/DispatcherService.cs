using Microsoft.Extensions.Options;
using Microsoft.Extensions.Hosting;
using ChatServer.Configuration;
using System.Threading.Tasks;
using ChatServer.Server;
using System.Threading;
using System;
using Microsoft.Extensions.Logging;
using ChatServer.Network;
using ChatServer.Network.Manager;

namespace ChatServer.Services
{
    public class DispatcherService : IHostedService, IDisposable
    {
        private Dispatcher Dispatcher { get; }
        
        private ILogger<DispatcherService> Logger { get; }
        
        private IOptions<ServerConfigurations> Configurations { get; } 

        private NetworkManagerFactory NetworkManager { get; }
        
        public DispatcherService(
            Dispatcher dispatcher,
            ILogger<DispatcherService> logger,
            IOptions<ServerConfigurations> configurations,
            NetworkManagerFactory networkManager)
        {
            Dispatcher = dispatcher;
            Logger = logger;
            Configurations = configurations;
            NetworkManager = networkManager;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation("DispatcherService starting");

            int port = Configurations.Value.Port;
            string networkOption = Configurations.Value.NetworkOption;
            
            if (port < 0)
                throw new IndexOutOfRangeException($"The port can not be set to '{port}', because it is less than zero");
            
            if (string.IsNullOrWhiteSpace(networkOption))
                throw new Exception($"NetworkOption configuration '{networkOption}' is not a valid configuration");
            
            Dispatcher.Start(port, NetworkManager.Build(networkOption));
            
            Task.Run(() => Dispatcher.Dispatch(), cancellationToken);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation("DispatcherService stopping");

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Dispatcher.Dispose();
        }
    }
}