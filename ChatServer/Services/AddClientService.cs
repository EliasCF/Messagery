using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using ChatServer.Network;
using ChatServer.Network.NetworkClient;
using ChatServer.Server;
using ChatServer.ServerClient;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ChatServer.Services
{
    public class AddClientService : BackgroundService
    {
        private ILogger<AddClientService> Logger { get; }
        
        private PendingNetworkClientsQueue NetworkClientsQueue { get; }

        private ClientHandler Clients { get; }
        
        private MessageSender Sender { get;  }
        
        private MessageReader Reader { get; }

        public AddClientService (
            ILogger<AddClientService> logger,
            PendingNetworkClientsQueue networkClientsQueue,
            ClientHandler clients,
            MessageSender sender,
            MessageReader reader)
        {
            Logger = logger;
            NetworkClientsQueue = networkClientsQueue;
            Clients = clients;
            Sender = sender;
            Reader = reader;
        }
        
        /// <summary>
        /// Subscribe to EnqueueEvent in PendingSocketsQueue
        /// </summary>
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Logger.LogInformation("AddClientService starting");

            NetworkClientsQueue.EnqueueEvent += AddClients;
            
            return Task.CompletedTask;
        }
        
        /// <summary>
        /// Unsubscribe to EnqueueEvent in PendingSocketsQueue
        /// </summary>
        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            Logger.LogInformation("AddClientService stopping");

            NetworkClientsQueue.EnqueueEvent -= AddClients;
            
            await base.StopAsync(stoppingToken);
        }

        /// <summary>
        /// Get the next client Socket from PendingSocketsQueue ready as an client
        /// </summary>
        private void AddClients(object sender, EventArgs e)
        {
            INetworkClient socket = NetworkClientsQueue.Dequeue();
            
            Guid newClient = Clients.Add(socket);
            StateObject state = new StateObject(Clients.GetId(newClient));
            
            Sender.Send(state.Client, "Welcome, you need to set your name by typing the name command: '/Name <name>' \r\nExample: '/Name Lars'\r\n");
            
            IAsyncResult result = state.Client.Connection
                .BeginReceive(state.Buffer, 0, StateObject.BufferSize,
                    new AsyncCallback(Reader.ReadCallback), state);

            if (result == null) 
            {
                Sender.SendToAll(null, $"{state.Client.Name} left the chat", true);
                Clients.Close(state.Client.Id);
            }
        }
    }
}