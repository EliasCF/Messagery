namespace ChatServer.Configuration
{
    public class Security
    {
        /// <summary>
        /// Path to the SSL certificate used for encryption on server communication
        /// </summary>
        /// <value></value>
        public string CertificatePath { get; set; }

        /// <summary>
        /// Certificate password
        /// </summary>
        public string CertificatePassword { get; set; }
    }
}