namespace ChatServer.Configuration
{
    public class ServerConfigurations
    {
        /// <summary>
        /// Indicates whether the server should be strict with resources.
        /// If Strict is true, all resources will be disposed of, as soon as they are unused.
        /// </summary>
        public bool Strict { get; set; }
    
        /// <summary>
        /// Port that the server will run on
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// The networking protocol to use on the server
        /// </summary>
        public string NetworkOption { get; set; }

        /// <summary>
        /// The security related settings for the server
        /// </summary>
        public Security Security { get; set; }
    }
}