using System;
using System.Linq;
using ChatServer.Network.Manager;
using Microsoft.Extensions.DependencyInjection;

namespace ChatServer.Extensions.Network
{
    public static class NetworkManagerServiceCollectionExtensions
    {
        /// <summary>
        /// Add all implementations of the INetworkManager interface to a IServiceCollection as Scoped services
        /// </summary>
        /// <param name="services">Service to add Scoped services to</param>
        /// <returns>IServiceCollection with add INetworkManager Scoped services</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddNetworkManagers(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof (services));

            Type interfaceType = typeof(INetworkManager);
            
            AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => interfaceType.IsAssignableFrom(p) && !p.IsInterface)
                .ToList()
                .ForEach((command) =>
                {
                    services.AddScoped(interfaceType, command);
                });

            return services;
        }
    }
}