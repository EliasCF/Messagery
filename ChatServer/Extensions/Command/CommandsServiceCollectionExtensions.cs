using System;
using System.Linq;
using ChatServer.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace ChatServer.Extensions.Command
{
    public static class CommandsServiceCollectionExtensions
    {
        /// <summary>
        /// Add all implementations of the ICommand interface to a IServiceCollection as Scoped services
        /// </summary>
        /// <param name="services">Service to add Scoped services to</param>
        /// <returns>IServiceCollection with add ICommand Scoped services</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddCommands(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof (services));

            Type type = typeof(ICommand);
            
            AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface)
                .ToList()
                .ForEach((command) =>
                {
                    services.AddScoped(typeof (ICommand), command);
                });

            return services;
        }
    }
}