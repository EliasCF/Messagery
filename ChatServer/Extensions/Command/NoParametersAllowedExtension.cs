using ChatServer.Server;
using ChatServer.ServerClient;

namespace ChatServer.Extensions.Command
{
    public static class NoParametersAllowedExtension
    {
        public static bool ContainsParameters (this string parameters, MessageSender sender, Client client, string command) 
        {
            if (parameters != null) 
            {
                sender.Send(client, "No parameters allowed\n\r");
                
                return true;
            }

            return false;
        }
    }
}
