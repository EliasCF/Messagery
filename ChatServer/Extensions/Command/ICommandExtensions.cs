using System;
using ChatServer.Commands;
using ChatServer.ServerClient;

namespace ChatServer.Extensions.Command
{
    public static class ICommandExtensions
    {
        /// <summary>
        /// Get all ClientStates that a certain command is compatible with, as specified by its CompatibleClientStates attribute.
        /// </summary>
        public static ClientState[] GetCompatibleClientStates(this ICommand command)
        {
            CompatibleClientStatesAttribute compatibleClientStates =
                (CompatibleClientStatesAttribute) Attribute.GetCustomAttribute(command.GetType(), typeof(CompatibleClientStatesAttribute));

            return compatibleClientStates.States;
        }

        public static bool IsCommandOutputIgnorable(this ICommand command)
        {
            OutputIgnorable outputIgnorable =
                (OutputIgnorable) Attribute.GetCustomAttribute(command.GetType(), typeof(OutputIgnorable));

            return outputIgnorable != null;
        }
    }
}