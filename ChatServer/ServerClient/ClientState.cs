namespace ChatServer.ServerClient
{
    public enum ClientState 
    {
        /// <summary>
        /// The client is free to do anything
        /// </summary>
        Passive,
        
        /// <summary>
        /// The Client must set it's name to continue
        /// </summary>
        NeedName
    }
}