using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Linq;
using System;
using ChatServer.Network.NetworkClient;

namespace ChatServer.ServerClient
{
    public class ClientHandler
    {
        private ILogger<ClientHandler> Logger { get; }

        private List<Client> Clients { get; set; }

        public ClientHandler (ILogger<ClientHandler> logger) 
        {
            Logger = logger;
            Clients = new List<Client>();
        }

        /// <summary>
        /// Current amount of connected clients
        /// </summary>
        /// <value></value>
        public int Count => Clients.Count;

        /// <summary>
        /// Add a newly accepted client
        /// </summary>
        /// <param name="socket">The new socket connection</param>
        /// <param name="name">Name of the user</param>
        public Guid Add (INetworkClient socket, string name = "") 
        {
            Logger.LogInformation($"Adding: '{socket.RemoteEndPoint}' to client list");
            
            //Initialize new Client object
            Guid id = Guid.NewGuid();
            Client newClient = new Client(id, socket, name, ClientState.NeedName);

            Clients.Add(newClient);

            return id;
        }

        /// <summary>
        /// Get a list of all connected client
        /// </summary>
        /// <returns>All connected clients</returns>
        public List<Client> GetAll () 
        {
            return Clients;
        }

        /// <summary>
        /// Get a particular client by their id
        /// </summary>
        /// <param name="id">Client id</param>
        /// <returns>The specified client</returns>
        public Client GetId (Guid id) 
        {
            return Clients.SingleOrDefault(c => c.Id.Equals(id));
        }

        /// <summary>
        /// Sets the name of a specific client
        /// </summary>
        /// <param name="id">The client's Guid identification</param>
        /// <param name="name">The name to give the client</param>
        public void SetName (Guid id, string name) 
        {
            int index = Clients.FindIndex(c => c.Id == id);

            if (index == -1) return;
            
            Logger.LogInformation($"Setting name of client: '{Clients[index].Connection.RemoteEndPoint}', to: '{name}',");
            Clients[index].Name = name;
        }

        /// <summary>
        /// Change the state of a client
        /// </summary>
        /// <param name="id">the client's Guid identification</param>
        /// <param name="newState">The state to change the current state to</param>
        public void SetState (Guid id, ClientState newState)
        {
            int index = Clients.FindIndex(c => c.Id == id);

            if (index == -1) return;
            
            Clients[index].State = newState;
        }

        /// <summary>
        /// Change the room that a client is connected to
        /// </summary>
        /// <param name="id">The client's Guid identification</param>
        /// <param name="room">The room's Guid identification</param>
        public void SetRoom (Guid id, Guid room) 
        {
            int index = Clients.FindIndex(c => c.Id == id);

            if (index == -1) return;
            
            Clients[index].RoomId = room;
        }

        /// <summary>
        /// Close a specific client
        /// </summary>
        /// <param name="id">The client's Guid identification</param>
        public void Close (Guid id) 
        {
            int index = Clients.FindIndex(c => c.Id == id);

            Logger.LogInformation($"Closing client: {Clients[index].Connection.RemoteEndPoint}");
            Clients[index].Connection.Close();
            Clients.RemoveAt(index);
        }

        /// <summary>
        /// Check if a specific Client exists
        /// </summary>
        /// <param name="id">The client's Guid identification</param>
        /// <returns>Does the client exist</returns>
        public bool Exists (Guid id) 
        {
            return Clients.SingleOrDefault(c => c.Id.Equals(id)) != null;
        }

        /// <summary>
        /// Check if a specifc Client exists
        /// </summary>
        /// <param name="name">The name of the client</param>
        /// <returns>Does the client exist</returns>
        public bool Exists (string name)
        {
            return Clients.SingleOrDefault(c => c.Name.Equals(name)) != null;
        }

        /// <summary>
        /// Get a particular client by their name
        /// </summary>
        /// <param name="name">Name of the client</param>
        /// <returns>The specified client</returns>
        public Client GetByName (string name)
        {
            return Clients.SingleOrDefault(c => c.Name == name);
        }
    }
}