using System;

namespace ChatServer.ServerClient
{
    /// <summary>
    /// Will be placed on ICommand implementations to have metadata about what ClientStates the command is compatible with
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class CompatibleClientStatesAttribute : Attribute
    {
        public ClientState[] States { get; }
        
        public CompatibleClientStatesAttribute(params ClientState[] states)
        {
            States = states;
        }
    }
}