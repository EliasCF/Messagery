using System.Net.Sockets;
using System.Linq;
using System;
using ChatServer.Network.NetworkClient;

namespace ChatServer.ServerClient
{
    public class Client
    {
        public Client (Guid newId, INetworkClient newConn, string newName, ClientState newState) 
        {
            Id = newId;
            Connection = newConn;
            Name = newName;
            State = newState;
            RoomId = Guid.Empty;
        }

        /// <summary>
        /// Guid for the client
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Client's network connection
        /// </summary>
        public INetworkClient Connection { get; }
        
        /// <summary>
        /// Identifying name of the client
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The state of the client
        /// </summary>
        public ClientState State { get; set; }

        /// <summary>
        /// The Guid of the room that the client is currently in.
        /// The Guid will be Guid.Empty if the client has not joined any room.
        /// </summary>
        public Guid RoomId { get; set; }
    }
}