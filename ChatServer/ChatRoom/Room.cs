using System;
using ChatServer.ServerClient;

namespace ChatServer.ChatRoom
{
    public class Room
    {   
        /// <summary>
        /// Guid for the room.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Name of the room.
        /// Will be used by user for identifying the room.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Guid of the client that created the room
        /// </summary>
        public Guid CreatedBy { get; set; }

        /// <summary>
        /// The time the room was created
        /// </summary>
        public DateTime CreationTime { get; set; }
        
        /// <summary>
        /// Amount of clients in the room
        /// </summary>
        public int ClientCount { get; private set; }
        
        /// <summary>
        /// Increment ClientCount property by one
        /// </summary>
        public void IncrementClientCount ()
        {
            ClientCount++;
        }
        
        /// <summary>
        /// Decrement ClientCount property by one
        /// </summary>
        public void DecrementClientCount()
        {
            ClientCount--;
        }
    }
}