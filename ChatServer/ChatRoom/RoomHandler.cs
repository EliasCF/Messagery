using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace ChatServer.ChatRoom
{
    public class RoomHandler
    {
        private List<Room> ChatRooms { get; set; }

        private ILogger<RoomHandler> Logger { get; }

        public RoomHandler (ILogger<RoomHandler> log) 
        {
            Logger = log;
            ChatRooms = new List<Room>();
        }

        /// <summary>
        /// Current amount of chat rooms
        /// </summary>
        /// <value></value>
        public int Count => ChatRooms.Count;

        /// <summary>
        /// Add a room to the list of rooms
        /// </summary>
        /// <param name="name">Name of the new room</param>
        /// <param name="creator">Id of the client who has created the room</param>
        public Guid Add (string name, Guid creator)
        { 
            if (ChatRooms.All(cr => cr.Name != name)) 
            {
                Logger.LogInformation($"Creating new chat room: {name}");

                Guid id = Guid.NewGuid();

                ChatRooms.Add(new Room 
                { 
                    Id = id,
                    Name = name,
                    CreatedBy = creator,
                    CreationTime = DateTime.Now
                });

                return id;
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Remove a room from the list of rooms
        /// </summary>
        /// <param name="id">Guid of room</param>
        public void Remove (Guid id) 
        {
            Room roomToRemove = ChatRooms.SingleOrDefault(r => r.Id == id);

            if (roomToRemove != null) {
                Logger.LogInformation($"Removing chat room: {roomToRemove.Name}");
                ChatRooms.Remove(roomToRemove);
            }
        }

        /// <summary>
        /// Get a list of all rooms
        /// </summary>
        /// <returns>All rooms</returns>
        public List<Room> GetAll ()
        {
            return ChatRooms;
        }

        /// <summary>
        /// Get a rooms id by its name
        /// </summary>
        /// <param name="name">Name of room</param>
        /// <returns>Room id</returns>
        public Guid FindByName (string name)
        {
            if (name == null) return Guid.Empty;

            if (!ChatRooms.Exists(cr => cr.Name == name)) return Guid.Empty;

            return ChatRooms.Single(cr => cr.Name == name).Id;
        }

        public Room FindRoomById(Guid id)
        {
            if (id == Guid.Empty) return null;

            return ChatRooms.SingleOrDefault(cr => cr.Id == id);
        }
        
        /// <summary>
        /// Increment the ClientCount of a given room
        /// </summary>
        /// <param name="id">Room id</param>
        public void IncrementClientCount(Guid id)
        {
            if (id == Guid.Empty) return;
            
            FindRoomById(id)?.IncrementClientCount();
        }
    
        /// <summary>
        /// Decrement the ClientCount of a given room
        /// </summary>
        /// <param name="id">Room id</param>
        public void DecrementClientCount(Guid id)
        {
            if (id == Guid.Empty) return;
            
            FindRoomById(id)?.DecrementClientCount();
        }
        
        /// <summary>
        /// Get the ClientCount of a given room
        /// </summary>
        /// <param name="id">Room id</param>
        /// <returns>ClientCount of given room</returns>
        public int GetClientCount(Guid id)
        {
            if (id == Guid.Empty) return -1;

            Room room = FindRoomById(id);

            if (room == null) return -1;

            return room.ClientCount;
        }
    }
}
