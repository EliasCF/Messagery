using System;
using System.Collections.Concurrent;
using ChatServer.Network.NetworkClient;

namespace ChatServer.Network
{
    public class PendingNetworkClientsQueue
    {
        private BlockingCollection<INetworkClient> SocketQueue { get; } = new BlockingCollection<INetworkClient>();

        public event EventHandler EnqueueEvent;

        /// <summary>
        /// Add socket to the queue
        /// </summary>
        /// <param name="socket">Socket to add</param>
        public void Enqueue (INetworkClient socket)
        {
            SocketQueue.Add(socket);
            OnEnqueueEvent();
        }
        
        /// <summary>
        /// Remove socket from queue
        /// </summary>
        /// <returns>The socket in the queue that was first added</returns>
        public INetworkClient Dequeue ()
        {
            if (SocketQueue.Count == 0) return null;

            INetworkClient socket = SocketQueue.Take();

            return socket;
        }

        /// <summary>
        /// Invoke EnqueueEvent to signal to event subscribers that a new socket has been enqueued
        /// </summary>
        private void OnEnqueueEvent()
        {
            EnqueueEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}