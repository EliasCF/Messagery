using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Logging;

namespace ChatServer.Network.Manager
{
    /// <summary>
    /// Exception for when the NetworkOption setting could not be found
    /// </summary>
    public class NetworkOptionNotFound : Exception
    {
        public NetworkOptionNotFound () { }

        public NetworkOptionNotFound (string message) : base (message) { }
    }

    public class NetworkManagerFactory
    {
        private ILogger<NetworkManagerFactory> Logger { get; }

        private IEnumerable<INetworkManager> NetworkManagers { get; }

        public NetworkManagerFactory (
            ILogger<NetworkManagerFactory> logger,
            IEnumerable<INetworkManager> networkManagers) 
        {
            Logger = logger;
            NetworkManagers = networkManagers;
        }

        public INetworkManager Build (string networkOptionName) 
        {
            INetworkManager configuredNetworkManager = NetworkManagers.SingleOrDefault((manager) => 
            {
                PropertyInfo networkOptionNameProperty = manager.GetType().GetProperty("NetworkOptionName");

                if (networkOptionNameProperty == null) return false;

                return networkOptionNameProperty
                    .GetValue(manager, null)
                    .ToString()
                    .Equals(networkOptionName);
            });

            if (configuredNetworkManager == null)
                throw new NetworkOptionNotFound($"The setting for NetworkOption: '{networkOptionName}', could not be found");
            
            Logger.LogInformation("");

            return configuredNetworkManager;    
        }
    }
}