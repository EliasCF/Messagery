using System;
using System.Net;
using System.Net.Sockets;
using ChatServer.Network.NetworkClient;
using Microsoft.Extensions.Logging;

namespace ChatServer.Network.Manager
{
    public class TcpNetworkManager : INetworkManager
    {
        public string NetworkOptionName => "Tcp";

        public event EventHandler ResetEventIsSet;
        
        private ILogger<TcpNetworkManager> Logger { get; }

        private TcpListener Listener { get; set; }
        
        private PendingNetworkClientsQueue NetworkClientsQueue { get; }

        public TcpNetworkManager (
            ILogger<TcpNetworkManager> log,
            PendingNetworkClientsQueue networkClientsQueue) 
        {
            Logger = log;
            NetworkClientsQueue = networkClientsQueue;
        }
        
        /// <summary>
        /// Configure the TCPListener for the server
        /// </summary>
        /// <param name="port">Port to run server on</param>
        public void Configure (int port) 
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            Listener = new TcpListener(ipHostInfo.AddressList[0], port);
        }
        
        /// <summary>
        /// Start listening on TCPListener
        /// </summary>
        public void ListenForConnections () 
        {
            Logger.LogInformation($"The server is now listening for connections on: {Listener.LocalEndpoint.ToString()}");
            Listener.Start();
        }
        
        /// <summary>
        /// Begin accepting Client
        /// </summary>
        public void AcceptClient ()
        {
            Listener.BeginAcceptTcpClient(GetClientSocket, Listener.Server);
        }
        
        /// <summary>
        /// Get client Socket and enqueue socket so that it can be added to the ClientHandler by the AddClientService
        /// </summary>
        /// <param name="result"></param>
        private void GetClientSocket(IAsyncResult result)
        {
            Socket listener = (Socket)result.AsyncState;
            Socket handler = listener.EndAccept(result);
            INetworkClient client = new TcpNetworkClient(handler);
            
            NetworkClientsQueue.Enqueue(client);
            
            OnManualResetEventSet(new EventArgs());
        }
        
        private void OnManualResetEventSet (EventArgs e)
        {
            EventHandler handler = ResetEventIsSet;
            handler?.Invoke(this, e);
        }
    }
}