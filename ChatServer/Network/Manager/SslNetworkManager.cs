using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using ChatServer.Configuration;
using ChatServer.Network.NetworkClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ChatServer.Network.Manager
{
    public class SslNetworkManager : INetworkManager
    {
        public string NetworkOptionName => "Ssl";

        private SslStream ssl { get; }

        public event EventHandler ResetEventIsSet;

        private TcpListener Listener { get; set; }

        private ILogger<SslNetworkManager> Logger { get; }

        private PendingNetworkClientsQueue NetworkClientsQueue { get; }

        private Security SecuritySettings { get; }

        private X509Certificate2 _serverCertificate = null;

        public SslNetworkManager (
            ILogger<SslNetworkManager> logger,
            PendingNetworkClientsQueue networkClientsQueue,
            IOptions<ServerConfigurations> configurations)
        {
            Logger = logger;
            NetworkClientsQueue = networkClientsQueue;
            SecuritySettings = configurations.Value.Security;
        }

        public void Configure(int port)
        {
            string certificatePath = SecuritySettings.CertificatePath;
            string certificatePassword = SecuritySettings.CertificatePassword;
            
            if (string.IsNullOrWhiteSpace(certificatePath) || string.IsNullOrWhiteSpace(certificatePassword))
                throw new Exception("A Security setting has not been configured");
            
            _serverCertificate = new X509Certificate2(certificatePath, certificatePassword);

            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            Listener = new TcpListener(ipHostInfo.AddressList[0], port);
        }

        public void ListenForConnections ()
        {
            Logger.LogInformation($"The server is now listening for connections on: {Listener.LocalEndpoint.ToString()}");
            Listener.Start();
        }

        public void AcceptClient()
        {
            Listener.BeginAcceptTcpClient(GetClientSocket, Listener.Server);
        }

        private void GetClientSocket(IAsyncResult result)
        {
            Socket listener = (Socket)result.AsyncState;

            TcpClient client = new TcpClient
            {
                Client = listener.EndAccept(result)
            };

            SslStream stream = new SslStream(client.GetStream(), false);
            stream.AuthenticateAsServer(_serverCertificate);

            INetworkClient networkClient = new SslNetworkClient(stream, client.Client.RemoteEndPoint?.ToString());

            NetworkClientsQueue.Enqueue(networkClient);
            
            OnManualResetEventSet(new EventArgs()); 
        }

        private void OnManualResetEventSet (EventArgs e)
        {
            EventHandler handler = ResetEventIsSet;
            handler?.Invoke(this, e);
        }
    }
}