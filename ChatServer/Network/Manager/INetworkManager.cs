using System;

namespace ChatServer.Network.Manager
{
    public interface INetworkManager
    {
        /// <summary>
        /// The name that will cause the implementation of the INetworkManager to be used by the server
        /// if specified in appsettings.json ServerConfigurations.NetworkOption.
        /// </summary>
        string NetworkOptionName { get; }

        event EventHandler ResetEventIsSet;
        
        void ListenForConnections ();

        void AcceptClient ();

        void Configure (int port);
    }
}
