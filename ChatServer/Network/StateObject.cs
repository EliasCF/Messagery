using ChatServer.ServerClient;
using System.Text;

namespace ChatServer.Network
{
    public class StateObject
    {
        /// <summary>
        /// Set client property
        /// </summary>
        /// <param name="socket">Socket to be set as the client property</param>
        public StateObject (Client c = null) 
        {
            Client = c;
        }

        public readonly Client Client;

        public const int BufferSize = 1024;

        public readonly byte[] Buffer = new byte[BufferSize];

        public readonly StringBuilder Builder = new StringBuilder();
    }
}