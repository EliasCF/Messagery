using System;

namespace ChatServer.Network.NetworkClient
{
    public interface INetworkClient
    {
        /// <summary>
        /// Get client ip address
        /// </summary>
        string RemoteEndPoint { get; }

        /// <summary>
        /// Close connection to client
        /// </summary>
        void Close();

        /// <summary>
        /// Begin receiving from the client
        /// </summary>
        IAsyncResult BeginReceive (byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState);

        /// <summary>
        /// End receiving from the client
        /// </summary>
        int EndReceive (IAsyncResult asyncResult);

        /// <summary>
        /// Begin sending to the client
        /// </summary>
        IAsyncResult BeginSend (byte[] buffer, int offset, int size, AsyncCallback callback, object state);
        
        /// <summary>
        /// End sending to the client
        /// </summary>
        int EndSend (IAsyncResult result);
    }
}