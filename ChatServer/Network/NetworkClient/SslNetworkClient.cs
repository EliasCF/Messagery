using System;
using System.Net.Security;

namespace ChatServer.Network.NetworkClient
{
    public class SslNetworkClient : INetworkClient
    {
        private SslStream Stream { get; }

        public SslNetworkClient (
            SslStream stream) 
        {
            Stream = stream;
            RemoteEndPoint = string.Empty;
        }

        public SslNetworkClient(
            SslStream stream,
            string remoteEndPoint)
        {
            Stream = stream;
            RemoteEndPoint = remoteEndPoint;
        }

        public string RemoteEndPoint { get; }

        public void Close()
        {
            Stream.Close();
        }

        public IAsyncResult BeginReceive(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
        {
            IAsyncResult result = null;
            
            try 
            {
                result = Stream.BeginRead(buffer, offset, count, asyncCallback, asyncState);      
            } 
            catch (Exception)
            {
                return null;
            }

            return result;  
        }

        public IAsyncResult BeginSend(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
        {
            IAsyncResult result = null;
            
            try 
            {
                result = Stream.BeginWrite(buffer, offset, size, callback, state);
            } 
            catch (Exception)
            {
                return null;
            }

            return result;
        }

        public int EndReceive(IAsyncResult asyncResult)
        {
            int result = -1;

            try 
            {
                result = Stream.EndRead(asyncResult);
            }
            catch(Exception) 
            {
                return -1;
            }

            return result;
        }

        public int EndSend(IAsyncResult result)
        {
            try 
            {
                Stream.EndWrite(result);
            }
            catch(Exception) 
            {
                return -1;
            }

            return 0;
        }
    }
}