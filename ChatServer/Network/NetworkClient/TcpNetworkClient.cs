using System;
using System.Net.Sockets;

namespace ChatServer.Network.NetworkClient
{
    public class TcpNetworkClient : INetworkClient
    {
        private Socket Socket { get; }

        public TcpNetworkClient (Socket socket) 
        {
            Socket = socket;
        }

        public string RemoteEndPoint => Socket.RemoteEndPoint?.ToString();

        public void Close()
        {
            Socket.Close();
        }

        public IAsyncResult BeginReceive(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
        {
            IAsyncResult result = null;
            
            try 
            {
                result = Socket.BeginReceive(buffer, offset, count, SocketFlags.None, asyncCallback, asyncState);
            } 
            catch (Exception)
            {
                return null;
            }

            return result;
        }

        public IAsyncResult BeginSend(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
        {
            IAsyncResult result = null;

            try 
            {
                result = Socket.BeginSend(buffer, offset, size, SocketFlags.None, callback, state);
            }
            catch (Exception)
            {
                return null;    
            }

            return result;
        }

        public int EndReceive(IAsyncResult asyncResult)
        {
            int result = -1;

            try 
            {
                result = Socket.EndReceive(asyncResult);
            }
            catch(Exception) 
            {
                return -1;
            }

            return result;
        }

        public int EndSend(IAsyncResult result)
        {
            int endResult = -1;

            try 
            {
                endResult = Socket.EndSend(result);
            }
            catch (Exception)
            {
                return -1;
            }

            return endResult;
        }
    }
}