using Microsoft.Extensions.Logging;
using ChatServer.Network;
using System.Threading;
using System;
using ChatServer.Network.Manager;

namespace ChatServer.Server 
{
    public class Dispatcher 
    {
        private INetworkManager Network { get; set; }

        private ILogger<Dispatcher> Logger { get; }

        private readonly ManualResetEvent _allDone = new ManualResetEvent(false);

        public Dispatcher (
            ILogger<Dispatcher> logger)
        {
            Logger = logger;
        }

        public void Start (int port, INetworkManager network) 
        {
            Network = network;

            Network.Configure(port);
        }

        /// <summary>
        /// Starts server and begins the acceptor loop
        /// </summary>
        public void Dispatch () 
        {
            Logger.LogInformation("Starting AcceptorLoop");
            AcceptorLoop();
        }

        /// <summary>
        /// Accept clients
        /// </summary>
        private void AcceptorLoop () 
        {
            Network.ListenForConnections();

            Network.ResetEventIsSet += SetManualResetEvent;

            while (true) 
            {  
                _allDone.Reset();
                Network.AcceptClient();
                _allDone.WaitOne();
            }
        }
        private void SetManualResetEvent (object sender, EventArgs e) 
        {
            _allDone.Set();
        }

        public void Dispose()
        {    
            Logger.LogInformation("Dispatcher disposing of resources");
        }
    }
}