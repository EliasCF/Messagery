using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using ChatServer.ServerClient;
using System.Linq;
using System.Text;
using System;

namespace ChatServer.Server
{
    public class MessageSender
    {
        private ILogger<MessageSender> Logger { get; }
        
        private ClientHandler Clients { get; }

        public MessageSender (
            ILogger<MessageSender> logger, 
            ClientHandler clients)
        {
            Logger = logger;
            Clients = clients;
        }

        /// <summary>
        /// Sends a text message to a specific client
        /// </summary>
        /// <param name="client">Client object of the client to receive the message</param>
        /// <param name="data"></param>
        public void Send (Client client, string data) 
        {
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            Logger.LogInformation($"Sending message: '{data}' to '{client.Name}'");
            
            IAsyncResult result = client.Connection.BeginSend(byteData, 0, byteData.Length,
                new AsyncCallback(SendCallback), client);

            if (result == null) Clients.Close(client.Id);
        }

        /// <summary>
        /// Send a text message to all connected clients
        /// </summary>
        /// <param name="from">The client who sent the message</param>
        /// <param name="message">Message to be sent</param>
        /// <param name="excludeSender">Whether or not the message should be sent back to the sender</param>
        public void SendToAll (Client from, string message, bool excludeSender) 
        {
            List<Client> sendTo = Clients.GetAll();

            string text = $"{message}\r\n";

            if (from != null) 
            {
                if (excludeSender) 
                {
                    sendTo = sendTo.Where(c => c.Id != from.Id && c.RoomId == from.RoomId).ToList();
                }
                
                text = $"{from.Name}: " + text;
            }

            foreach (Client client in sendTo) 
            {
                Send(client, text);
            }
        }

        /// <summary>
        /// Async callback for send method
        /// </summary>
        /// <param name="result">Client object of the receiving client</param>
        private void SendCallback (IAsyncResult result) 
        {
            Client client = (Client)result.AsyncState;

            int byteSent = client.Connection.EndSend(result);
            
            if (byteSent == -1) Clients.Close(client.Id);
        }
    }
}