using Microsoft.Extensions.Logging;
using ChatServer.Commands;
using ChatServer.ServerClient;
using ChatServer.Network;
using System.Text;
using System;
using System.Linq;
using ChatServer.Extensions.Command;

namespace ChatServer.Server
{
    public class MessageReader
    {
        private ILogger<MessageReader> Logger { get; }

        private MessageSender Sender { get; }

        private ClientHandler Clients { get; }

        private CommandFactory Factory { get; }

        public MessageReader (
            ILogger<MessageReader> logger, 
            MessageSender sender, 
            ClientHandler clients, 
            CommandFactory factory) 
        {
            Logger = logger;
            Sender = sender;
            Clients = clients;
            Factory = factory;
        }

        /// <summary>
        /// Read message from a client
        /// </summary>
        /// <param name="result">StateObject containing the client that is being read from</param>
        public void ReadCallback (IAsyncResult result) 
        {      
            StateObject state = (StateObject)result.AsyncState;

            Logger.LogInformation($"Reading message from: '{state.Client.Connection.RemoteEndPoint}'");

            int byteRead = state.Client.Connection.EndReceive(result);

            if (byteRead <= 0) 
            {
                Sender.SendToAll(null, $"{state.Client.Name} left the chat", true);
                Clients.Close(state.Client.Id);
                return;
            }
            
            state.Builder.Append(Encoding.ASCII.GetString(state.Buffer, 0, byteRead));
            string content = state.Builder.ToString();

            if (content.IndexOf("<EOF>", StringComparison.Ordinal) > -1)
            {
                Logger.LogInformation($"Read {content.Length} bytes from socket. \nData: '{content.Substring(0, content.Length - 6)}'");

                string message = content.Substring(0, content.Length - 6);

                bool ignoreCommandOutput = false;

                if (message.Length > 0 && message.ElementAt(0) == '/')
                {
                    ICommand command = Factory.Build(message);

                    if (command.IsCommandOutputIgnorable()) ignoreCommandOutput = true;
                    
                    if (command.GetCompatibleClientStates().Any(clientState => clientState == state.Client.State))
                    {
                        string parameters = null;
                        
                        if (message.Length != command.Command.Length) 
                        {
                            parameters = message.Substring(command.Command.Length + 1);
                        } 
                    
                        command.Handle(state.Client, parameters);
                        
                        //Exit method if the last command was a DiconnectCommand
                        if (!Clients.Exists(state.Client.Id)) return;
                    }
                }
                
                if (Clients.GetId(state.Client.Id).State != ClientState.NeedName && !ignoreCommandOutput) 
                {
                    Sender.SendToAll(state.Client, message, true); //Send message to all clients but the sender
                }

                state.Builder.Clear(); //Clear StringBuilder of messages
            }

            IAsyncResult asyncResult = state.Client.Connection
                .BeginReceive(state.Buffer, 0, StateObject.BufferSize,
                    new AsyncCallback(ReadCallback), state);

            if (asyncResult == null) 
            {
                Clients.Close(state.Client.Id);
                return;
            }
        }
    }
}