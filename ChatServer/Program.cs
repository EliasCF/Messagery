﻿using System.Threading.Tasks;
using ChatServer.ChatRoom;
using ChatServer.Commands;
using ChatServer.Configuration;
using ChatServer.Extensions.Command;
using ChatServer.Extensions.Network;
using ChatServer.Network;
using ChatServer.Network.Manager;
using ChatServer.Server;
using ChatServer.ServerClient;
using ChatServer.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ChatServer
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            await CreateHostBuilder(args).RunConsoleAsync();
        }

        static IHostBuilder CreateHostBuilder(string[] args)
        {
            var host = new HostBuilder()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile("appsettings.json", optional: true);
                    config.AddEnvironmentVariables();

                    if (args != null)
                    {
                        config.AddCommandLine(args);
                    }
                })
                .ConfigureServices((context, services) =>
                {
                    services.AddOptions();

                    services.Configure<ServerConfigurations>(context.Configuration.GetSection("ServerConfigurations"));

                    services.AddCommands();
                    services.AddScoped<CommandFactory>();

                    services.AddNetworkManagers();
                    services.AddScoped<NetworkManagerFactory>();

                    services.AddSingleton<PendingNetworkClientsQueue>();

                    services.AddScoped<MessageSender>();
                    services.AddScoped<MessageReader>();
                    services.AddSingleton<ClientHandler>();
                    services.AddSingleton<RoomHandler>();

                    services.AddScoped<Dispatcher>();

                    services.AddHostedService<AddClientService>();
                    services.AddHostedService<DispatcherService>();
                })
                .ConfigureLogging((context, logging) => { logging.AddConsole(); });

            return host;
        }
    }
}
